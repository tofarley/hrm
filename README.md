# hrm

Human Resource Machine Solutions


filenames:
- ##/solution.hrm        ; solution meets both size and speed challenges.
- ##/size-challenge.hrm  ; solution meets only the size challenge.
- ##/speed-challenge.hrm ; solution meeds only the speed challenge.
- ##/wip.hrm             ; solution succeeds but meets no challenges.
