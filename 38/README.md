# 38 - Digit Exploder

Grab each number from the INBOX, and send its digits to the OUTBOX. For example, 123 becomes 1, 2, 3.
