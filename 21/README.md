# 21 - Zero Terminated Sum

The INBOX is filled with zero terminated strings! What's that? Ask me. Your Boss.

Add together all the numbers in each string. When you reach the end of a string (marked by a ZERO), put your sum in the OUTBOX. Reset and repeat for each string.