# 35 - Duplicate Removal

Send everything from the INBOX to the OUTBOX, unless you've seen the same value before. Discard any duplicates.